#!/usr/bin/env ruby
# coding: utf-8
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2019 Jonathan Riddell <sitter@kde.org>

require 'nokogiri'
require 'optparse'

class MetaInfoUpdater
  attr_accessor :filename
  attr_accessor :doc
  attr_accessor :version
  attr_accessor :date_string
  attr_accessor :releases_to_show
  attr_accessor :date_off

  def initialize(filename, version, date_string = 'today', date_off = false, releases_to_show = 0)
    @filename = filename
    @version = version
    @date_string = date_string
    @date_off = date_off
    @releases_to_show = releases_to_show
  end

  def date
    `date --iso-8601 --date='#{@date_string}'`.chomp!
  end

  def open_file
    abort 'No such file' unless File.exist?(@filename)

    @doc = File.open(@filename) { |f| Nokogiri::XML(f, &:noblanks) }
    component = @doc.at_css('component')
    releases = @doc.at_css('releases')
    if releases && @date_off
      releases.prepend_child("<release version='#{@version}'/>")
    elsif !releases && @date_off
      component.add_child("<releases><release version='#{@version}'/></releases>")
    elsif releases && !@date_off
      releases.prepend_child("<release version='#{@version}' date='#{date}'/>")
    else
      component.add_child("<releases><release version='#{@version}' date='#{date}'/></releases>")
    end
    if releases && @releases_to_show > 0
      if releases.children.length > @releases_to_show
        #releases.children = releases.children[-@releases_to_show,@releases_to_show]
        releases.children = releases.children[0, @releases_to_show]
      end
    end
  end

  def save_file
    save_file_name = @filename
    f = File.open(save_file_name, 'w')
    @doc.write_xml_to(f, {indent: 2})
    f.close
  end
end
