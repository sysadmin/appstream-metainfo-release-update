# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2019 Jonathan Riddell <sitter@kde.org>

require 'minitest/autorun'

require 'fileutils'
require 'tmpdir'

require_relative '../lib/appstream-metainfo-release-update'

class MetaInfoUpdaterTest < MiniTest::Test
  def setup
    @tmpdir = Dir.mktmpdir
    Dir.chdir(@tmpdir)
    FileUtils.cp_r(Dir.glob("#{__dir__}/data/*"), @tmpdir)
  end

  def teardown
    FileUtils.rm_rf(@tmpdir)
  end

  # adding a release to a file with no releases
  def test_no_releases
    file = 'no-releases'
    updater = MetaInfoUpdater.new("#{file}.appdata.xml", '1.0', '2018-01-01', false)
    updater.open_file
    updater.save_file
    assert_equal(File.read("#{file}.appdata.xml.expected"), File.read("#{file}.appdata.xml"))
  end

  # date off
  def test_no_releases_date_off
    file = 'no-releases-date-off'
    updater = MetaInfoUpdater.new("#{file}.appdata.xml", '1.0', 'today', true)
    updater.open_file
    updater.save_file
    assert_equal(File.read("#{file}.appdata.xml.expected"), File.read("#{file}.appdata.xml"))
  end

  # adding a release to a file with 1 release
  def test_one_release
    file = 'one-release'
    updater = MetaInfoUpdater.new("#{file}.appdata.xml", '2.0', '2018-01-01', false)
    updater.open_file
    updater.save_file
    assert_equal(File.read("#{file}.appdata.xml.expected"), File.read("#{file}.appdata.xml"))
  end

  # adding a release to a file with 5 releases and limit set to 4
  def test_five_releases
    file = 'five-releases'
    updater = MetaInfoUpdater.new("#{file}.appdata.xml", '6.0', '2018-01-01', false, 4)
    updater.open_file
    updater.save_file
    assert_equal(File.read("#{file}.appdata.xml.expected"), File.read("#{file}.appdata.xml"))
  end
end
