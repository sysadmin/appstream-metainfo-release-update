# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2019 Jonathan Riddell <sitter@kde.org>

# appstream-metainfo-release-update

Updates AppStream Metainfo files for new releases of your software
